import React, { Component } from 'react';
import { scrollTop } from '../utils/Commons';
import { __FOOTERLOGO as FooterLogo, __TOTOP as ToTop } from '../utils/Constants';

export class Footer extends Component {
  render() {
    return (
      <div className="Footer">
        <div className="widgets">
          <div>
            <div className="left inner">
              <img src={ FooterLogo } alt="logo" />
              <p>サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト</p>
            </div>
            <div className="right inner">
              <img onClick={ scrollTop } className="totop" src={ToTop} alt="To Top" />
            </div>
          </div>
        </div>
        <div className="copyright">Copyright&copy;2007-2019 Blog Inc.</div>
      </div>
    );
  }
}

export default Footer;
