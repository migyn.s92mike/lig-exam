import React, { Component } from 'react';
import BlogComments from './BlogComments';
import BlogWriteComments from './BlogWriteComments';
import { __BlogSinglePost as singlePost } from '../utils/Constants';
import { uniqueKeyGen } from '../utils/Commons';

export class BlogPage extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      singlePost: {
        title: '',
        date: '',
        imgUrl: '',
        post: "",
        comments: []
      }
    }
  }
  
  componentDidMount() {
    this.setState({ singlePost });
  }

  updateComments() {
    const { singlePost: { comments }, singlePost } = this.state;
    const post = document.getElementById('comments').value;
    if(post.length) {
      let newComment = {
        date: "Today",
        comments: post
      }
      this.setState({
        singlePost: { 
          ...singlePost, 
          comments: [ 
            ...comments, 
            newComment 
          ]
        } 
      });
      document.getElementById('comments').value="";
    }
  }

  render() {
    const { 
      title,
      date,
      imgUrl,
      post,
      comments
    } = this.state.singlePost;
    return (
      <div className="BlogPage">
        <div className="breadcrums">
          <div className="box">
            <p onClick={this.props.onClick} >HOME &gt; <span>{ title }</span></p>
          </div>
        </div>
        <div className="box">
          <div className="post">
            <h6>{date}</h6>
            <h1>{ title }</h1>
            <img src={ imgUrl } alt=""/>
            <p dangerouslySetInnerHTML={{ __html: post }} />
          </div>
          <div className="comments">
            <h2>COMMENT</h2>
            { comments.map((comments)=> <BlogComments key={uniqueKeyGen('cmsfmla')} comments={ comments } />) }
            <BlogWriteComments onClick={ this.updateComments.bind(this) } />
          </div>
        </div>
      </div>
    )
  }
}

export default BlogPage;
