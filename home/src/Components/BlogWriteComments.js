import React, { Component } from 'react';

export class BlogWriteComments extends Component {
  render() {
    return (
      <div className="BlogWriteComments">
        <textarea name="comments" id="comments" cols="30" rows="10" placeholder="Write comment"></textarea>
        <button onClick={ this.props.onClick } className="button submit black right">Submit</button>
      </div>
    )
  }
}

export default BlogWriteComments;
