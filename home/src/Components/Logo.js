import React, { Component } from 'react';
import { __LOGOIMG as LogoImg } from '../utils/Constants';

export class Logo extends Component {
    render() {
        return (
            <img className="logo" onClick={this.props.onClick} src={ LogoImg } alt="Logo" />
        );
    }
}

export default Logo;
