import React, { Component } from 'react';

export class BlogComments extends Component {
  render() {
    const { comments, date } = this.props.comments;
    return (
      <div className="BlogComments">
        <p>{ comments }</p>
        <span className="date">{ date }</span>
      </div>
    )
  }
}

export default BlogComments;
