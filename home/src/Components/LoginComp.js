import React, { Component } from 'react';

export class LoginComp extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
       toSlide: {
        top:'-650px'
       }
    }
  }
  
  componentDidMount() {
    setTimeout(()=>{
      this.setState({ 
        toSlide: {
          top: 0,
          transition: 'top 2s ease 0s'
        } 
      });
    }, 500);
  }

  registerSlide() {
    this.setState({ toSlide: { top: '-800px', opacity: 0, transition: 'opacity 2s ease-in-out' } });
    this.slideDown('500');
    this.props.registerClick();
  }

  loginSlide() {
    this.setState({ toSlide: { top: '-650px', opacity: 0, transition: 'opacity 2s ease-in-out' } });
    this.slideDown('500');
    this.props.returnClick();
  }

  slideDown(delay) {
    setTimeout(()=>{
      this.setState({ 
        toSlide: {
          top: 0,
          transition: 'top 2s ease 0s'
        } 
      });
    }, delay);
  }
  
  render() {
    const { toSlide } = this.state;
    let displayConfirm = "";
    let displayTitle = this.props.type === 3 ? "Register" : "Login";
    let displayText = (<div className="register" onClick={ this.registerSlide.bind(this) }>No account yet? <span>REGISTER HERE</span></div>)
    if (this.props.type === 3) {
      displayConfirm = (<div><h6>Confirm Password</h6><input type="password" className="password2"/></div>);
      displayText = (<div className="register" onClick={ this.loginSlide.bind(this) }>Already have an account? <span>LOGIN HERE</span></div>);
    }
    return (
      <div className="LoginComp" style={ toSlide }>
        <h2>{ displayTitle }</h2>
        <div className="formLogin">
          <h6>Email</h6>
          <input type="email" className="email"/>
          <h6>Password</h6>
          <input type="password" className="password"/>
          { displayConfirm }
          <button className="button login black center">{ displayTitle }</button>
          { displayText }
        </div>
      </div>
    )
  }
}

export default LoginComp;
