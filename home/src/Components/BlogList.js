import React, { Component } from 'react';
import BlogItem from './BlogItem';
import { uniqueKeyGen } from '../utils/Commons';
import { 
  __BLOGLIST as blogPostStatic, 
  __LOADBLOG as loadPostStatic 
} from '../utils/Constants';

export class BlogList extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      tempMaxblogs: 2,
      tempCurrentblogs: 1,
       blogPost: [
        {
          imgurl: '',
          date: '',
          post: ''
        }
       ]
    }
  }
  
  componentDidMount() {
    this.setState({ blogPost: blogPostStatic });
  }

  onLoadMore() {
    const { blogPost, tempCurrentblogs } = this.state;
    this.setState({ tempCurrentblogs: tempCurrentblogs + 1, blogPost: [...blogPost, ...loadPostStatic]  });
  }

  displayLoadMore() {
    // let buttonMe = "";
    // if(this.state.tempCurrentblogs < this.state.tempMaxblogs) {
    //   buttonMe = <button onClick={ this.onLoadMore.bind(this) } className="button load-more black center">Load More</button>;
    // } 
    return (this.state.tempCurrentblogs < this.state.tempMaxblogs?<button onClick={ this.onLoadMore.bind(this) } className="button load-more black center">Load More</button>:"");
  }
  
  render() {
    const { blogPost } = this.state;
    const { onClick } = this.props;
    

    return (
      <div className="BlogList">
        <h2>News</h2>
        <div className="list">
          {
            blogPost.map((post)=>(<BlogItem key={ uniqueKeyGen('BPfmla') } onClick={ onClick } info={ post }/>))
          }
        </div>
        { this.state.tempCurrentblogs<this.state.tempMaxblogs?<button onClick={ this.onLoadMore.bind(this) } className="button load-more black center">Load More</button>:"" }
      </div>
    )
  }
}

export default BlogList;
