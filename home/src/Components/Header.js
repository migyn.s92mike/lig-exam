import React, { Component } from 'react';
import LoginButton from './LoginButton';
import Logo from './Logo';

export class Header extends Component {
  render() {
    const { onClick, loginClick, login } = this.props;
    return (
      <div className="Header">
        <div>
          <ul>
            <li><Logo onClick={onClick} /></li>
            <li><LoginButton onClick={loginClick } login={login} /></li>
          </ul>
        </div>
      </div>
    );
  }
}

export default Header;
