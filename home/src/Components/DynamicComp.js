import React, { Component } from 'react';
import Slider from './Slider';
import LoginComp from './LoginComp';

export class DynamicComp extends Component {
  render() {
    const { type, returnClick, registerClick } = this.props;
    let displayComp = <Slider />;
    if (type !== 1) {
      displayComp = <LoginComp 
        returnClick={ returnClick } 
        registerClick={ registerClick } 
        type={ type } />;
    }
    return (
      displayComp
    );
  }
}

export default DynamicComp;
