import React, { Component } from 'react';
import {
  __LEFTARROW as leftArrow,
  __RIGHTARROW as rightArrow,
  __ELLIPSE as ellipse,
  __ACTIVEELLIPSE as activeEllipse,
  __SLIDERCONTENT as slides
} from '../utils/Constants';
import { uniqueKeyGen } from '../utils/Commons';

let sliderevent;
let animationevent;
let defaultSlides = [
  {
    imgurl: "",
    date: "",
    content: ['']
  }
];
export class Slider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      slides: defaultSlides,
      currentSlide: 0,
      animation: { opacity: 1 }
    }
  }

  componentDidMount() {
    this.setState({ slides });
    // slider 7 second slide change
    sliderevent = setInterval(()=>{
      let { currentSlide } = this.state;
      if(currentSlide === this.state.slides.length-1 ) {
        currentSlide = -1;
      }
      this.setState({
        currentSlide: currentSlide+1,
        animation: { opacity: 0 }
      });
      this.opacityAnimation(500);
    }, 7000);
  }

  componentWillUnmount() {
    clearTimeout(animationevent);
    clearInterval(sliderevent);
    this.setState({ slides: defaultSlides });
  }

  updateSlide(direction) {
    let { currentSlide, slides } = this.state;
    if (currentSlide >= 0 && currentSlide <= slides.length-1) {
      if((direction && currentSlide<2) || (!direction && currentSlide>0)) {
        this.setState({
          currentSlide: direction ? currentSlide+1 : currentSlide-1,
          animation: { opacity: 0 }
        });
        this.opacityAnimation(500);
      }
    }
  }

  updateDirectSlide(content) {
    if(content !== this.state.currentSlide) {
      this.setState({ currentSlide: content, animation: { opacity: 0 } });
      this.opacityAnimation(500);
    }
  }

  opacityAnimation(delay) {
    clearTimeout(animationevent);
    animationevent = setTimeout(()=>{
      this.setState({ animation: { opacity: 1, transition: 'opacity 2s ease-in-out' } });
    }, delay);
  }

  render() {
    const { slides, currentSlide, animation } = this.state;
    const { content, date, imgurl } = slides[currentSlide];
    let counter = 0, counter2 = 0;
    return (
      <div className="Slider">
        <img style={ animation } src={ imgurl } alt="slider" />
        <div className="slider-design">
          <div className="slide">
            <div className="content" style={animation}>
              <ul>
              { content.map(item=>(<li key={ uniqueKeyGen('ukfmla') + counter++ } ><span>{ item }</span></li>)) }
              </ul>
              <p>{ date }</p>
            </div>
            <div className="navigation">
              <div className="arrows">
                <img onClick={ this.updateSlide.bind(this, false) } className="left" src={ leftArrow } alt="" />
                <img onClick={ this.updateSlide.bind(this, true) } className="right" src={ rightArrow }  alt="" />
              </div>
              <div className="ellipse">
                { 
                  slides.map(item=>{
                    let srcImg = counter2===currentSlide?activeEllipse: ellipse;
                    let tempCounter = counter2;
                    counter2++;
                    return (<img 
                      key={ uniqueKeyGen('spfmla') + counter++ } 
                      onClick={ this.updateDirectSlide.bind(this, tempCounter) } 
                      src={ srcImg } alt="" />);
                  }) 
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Slider;
