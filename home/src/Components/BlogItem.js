import React, { Component } from 'react';


export class BlogItem extends Component {
  render() {
    const { 
      imgurl, 
      date, 
      post
    } = this.props.info;
    return (
      <div className="item" onClick={ this.props.onClick }>
        <img src = { imgurl } alt=""/>
        <span className="date">{ date }</span>
        <p>{ post }</p>
      </div>
    )
  }
}

export default BlogItem;
