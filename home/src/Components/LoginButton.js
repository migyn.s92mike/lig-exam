import React, { Component } from 'react';

export class LoginButton extends Component {
  render() {
    return (
      <button onClick={this.props.onClick}>
        {this.props.login ? 'Close' : 'Login'}
      </button>
    );
  }
}

export default LoginButton;
