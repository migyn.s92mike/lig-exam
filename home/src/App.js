import React from 'react';
import './App.css';
import Header from './Components/Header';
import DynamicComp from './Components/DynamicComp';
import Footer from './Components/Footer';
import BlogList from './Components/BlogList';
import BlogPage from './Components/BlogPage';

export class App extends React.Component {
  constructor(props) {
    super(props);  

    // Legend type:
    // 1 = Slider
    // 2 = Login
    // 3 = Register
    // 4 = Successfull register
    this.state = {
      currentPage: 'home',
      login: false,
      type: 1,
    }
    this.onClickBlogPage = this.onClickBlogPage.bind(this);
    this.onClickHomePage = this.onClickHomePage.bind(this);
    this.loginClick = this.loginClick.bind(this);
    this.registerClick = this.registerClick.bind(this);
    this.returnClick = this.returnClick.bind(this);
  }
  onClickBlogPage=()=>{
    this.setState({ currentPage: 'blog', 
      login: false, type: 1 });
  }
  onClickHomePage=()=>{
    this.setState({ currentPage: 'home' });
  }
  loginClick=()=>{
    let { currentPage } = this.state;
    if (currentPage === 'blog') {
      currentPage = 'home';
    }
    this.setState({ 
      login: this.state.login?false:true, 
      type: this.state.login?1:2, 
      currentPage });
  }
  registerClick=()=>{
    this.setState({ type: 3 });
  }
  returnClick=()=>{
    this.setState({ type: 2 });
  }
  ToDisplay=()=>{
    const { currentPage, type } = this.state;
    if (currentPage === 'home') {
      return (
        <div className="container">
          <DynamicComp 
            returnClick={ this.returnClick } 
            registerClick={ this.registerClick } 
            type={ type } />
          <BlogList onClick={this.onClickBlogPage} />
        </div>
      );
    } else if (currentPage === 'blog') {
      return (
        <div className="container">
          <BlogPage onClick={ this.onClickHomePage } />
        </div>
      );
    }
  }
  render() {
    return (
      <div className="App">
        <Header 
          onClick={ this.onClickHomePage } 
          loginClick={ this.loginClick } 
          login={ this.state.login } />
        { this.ToDisplay() }
        <Footer />
      </div>
    );
  }
}

export default App;
