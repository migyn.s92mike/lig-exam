import sliderSrc1 from '../imgs/slider-1.jpg';
import sliderSrc2 from '../imgs/slider-2.jpg';
import sliderSrc3 from '../imgs/slider-3.jpg';
import leftArrow from '../imgs/left-arrow.svg';
import rightArrow from '../imgs/right-arrow.svg';
import ellipse from '../imgs/inactive-ellipse.svg';
import activeEllipse from '../imgs/active-ellipse.svg';
import LogoImg from '../imgs/logo.svg';
import FooterLogo from '../imgs/footer-logo.svg';
import ToTop from '../imgs/to-top.svg';
import BlogImage from '../imgs/blog-img.jpg';

export const __SLIDERSRC = sliderSrc1;
export const __LEFTARROW = leftArrow;
export const __RIGHTARROW = rightArrow;
export const __ELLIPSE = ellipse;
export const __ACTIVEELLIPSE = activeEllipse;
export const __LOGOIMG = LogoImg;
export const __FOOTERLOGO = FooterLogo;
export const __TOTOP = ToTop;
export const __LOADBLOG = [
  {
    imgurl: BlogImage,
    date: '2019.05.04',
    post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
  },
  {
    imgurl: BlogImage,
    date: '2019.05.04',
    post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
  },
  {
    imgurl: BlogImage,
    date: '2019.05.04',
    post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
  },
  {
    imgurl: BlogImage,
    date: '2019.05.04',
    post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
  },
  {
    imgurl: BlogImage,
    date: '2019.05.04',
    post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
  },
  {
    imgurl: BlogImage,
    date: '2019.05.04',
    post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
  }
];
export const __BLOGLIST = [
  {
    imgurl: BlogImage,
    date: '2019.06.19',
    post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
  },
  {
    imgurl: BlogImage,
    date: '2019.06.19',
    post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
  },
  {
    imgurl: BlogImage,
    date: '2019.06.19',
    post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
  },
  {
    imgurl: BlogImage,
    date: '2019.06.19',
    post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
  },
  {
    imgurl: BlogImage,
    date: '2019.06.19',
    post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
  },
  {
    imgurl: BlogImage,
    date: '2019.06.19',
    post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
  }
];
export const __SLIDERCONTENT = [
  {
    imgurl: sliderSrc1,
    date: '2019.06.19',
    content: [
      'サンプルテキスト',
      'サンプル ルテキストサン',
      'プルテキスト'
    ]
  },
  {
    imgurl: sliderSrc2,
    date: '2019.07.21',
    content: [
      'サンプル ルテキストサン',
      'サンプルテキスト',
      'プルテキスト'
    ]
  },
  {
    imgurl: sliderSrc3,
    date: '2019.08.22',
    content: [
      'プルテキスト',
      'サンプルテキスト',
      'サンプル ルテキストサン'
    ]
  }
];
export const __BlogSinglePost = {
  title: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト',
  date: '2019.06.19',
  imgUrl: sliderSrc1,
  post: "ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。<br /><br /> ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。",
  comments: [
    {
      comments: "ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。",
      date: '3 months ago'
    },
    {
      comments: "ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。",
      date: '3 months ago'
    }
  ]
};