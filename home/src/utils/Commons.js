export const uniqueKeyGen=(uniqueName)=> {
  return uniqueName + Math.floor(Math.random() * Math.floor(10000));
}

export const scrollTop=()=>{
  window.scrollTo({top: 0, behavior: 'smooth'});
};