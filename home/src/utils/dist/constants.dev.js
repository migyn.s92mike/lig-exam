"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.__BlogSinglePost = exports.__SLIDERCONTENT = exports.__BLOGLIST = exports.__LOADBLOG = exports.__TOTOP = exports.__FOOTERLOGO = exports.__LOGOIMG = exports.__ACTIVEELLIPSE = exports.__ELLIPSE = exports.__RIGHTARROW = exports.__LEFTARROW = exports.__SLIDERSRC = void 0;

var _slider = _interopRequireDefault(require("../imgs/slider-1.jpg"));

var _slider2 = _interopRequireDefault(require("../imgs/slider-2.jpg"));

var _slider3 = _interopRequireDefault(require("../imgs/slider-3.jpg"));

var _leftArrow = _interopRequireDefault(require("../imgs/left-arrow.svg"));

var _rightArrow = _interopRequireDefault(require("../imgs/right-arrow.svg"));

var _inactiveEllipse = _interopRequireDefault(require("../imgs/inactive-ellipse.svg"));

var _activeEllipse = _interopRequireDefault(require("../imgs/active-ellipse.svg"));

var _logo = _interopRequireDefault(require("../imgs/logo.svg"));

var _footerLogo = _interopRequireDefault(require("../imgs/footer-logo.svg"));

var _toTop = _interopRequireDefault(require("../imgs/to-top.svg"));

var _blogImg = _interopRequireDefault(require("../imgs/blog-img.jpg"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var __SLIDERSRC = _slider["default"];
exports.__SLIDERSRC = __SLIDERSRC;
var __LEFTARROW = _leftArrow["default"];
exports.__LEFTARROW = __LEFTARROW;
var __RIGHTARROW = _rightArrow["default"];
exports.__RIGHTARROW = __RIGHTARROW;
var __ELLIPSE = _inactiveEllipse["default"];
exports.__ELLIPSE = __ELLIPSE;
var __ACTIVEELLIPSE = _activeEllipse["default"];
exports.__ACTIVEELLIPSE = __ACTIVEELLIPSE;
var __LOGOIMG = _logo["default"];
exports.__LOGOIMG = __LOGOIMG;
var __FOOTERLOGO = _footerLogo["default"];
exports.__FOOTERLOGO = __FOOTERLOGO;
var __TOTOP = _toTop["default"];
exports.__TOTOP = __TOTOP;
var __LOADBLOG = [{
  imgurl: _blogImg["default"],
  date: '2019.05.04',
  post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
}, {
  imgurl: _blogImg["default"],
  date: '2019.05.04',
  post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
}];
exports.__LOADBLOG = __LOADBLOG;
var __BLOGLIST = [{
  imgurl: _blogImg["default"],
  date: '2019.06.19',
  post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
}, {
  imgurl: _blogImg["default"],
  date: '2019.06.19',
  post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
}, {
  imgurl: _blogImg["default"],
  date: '2019.06.19',
  post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
}, {
  imgurl: _blogImg["default"],
  date: '2019.06.19',
  post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
}, {
  imgurl: _blogImg["default"],
  date: '2019.06.19',
  post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
}, {
  imgurl: _blogImg["default"],
  date: '2019.06.19',
  post: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト'
}];
exports.__BLOGLIST = __BLOGLIST;
var __SLIDERCONTENT = [{
  imgurl: _slider["default"],
  date: '2019.06.19',
  content: ['サンプルテキスト', 'サンプル ルテキストサン', 'プルテキスト']
}, {
  imgurl: _slider2["default"],
  date: '2019.07.21',
  content: ['サンプル ルテキストサン', 'サンプルテキスト', 'プルテキスト']
}, {
  imgurl: _slider3["default"],
  date: '2019.08.22',
  content: ['プルテキスト', 'サンプルテキスト', 'サンプル ルテキストサン']
}];
exports.__SLIDERCONTENT = __SLIDERCONTENT;
var __BlogSinglePost = {
  title: 'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト',
  date: '2019.06.19',
  imgUrl: _slider["default"],
  post: "ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。<br /><br /> ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。",
  comments: [{
    comments: "ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。",
    date: '3 months ago'
  }, {
    comments: "ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。ここにはテキストが入ります。ここにはテキストが入りますここにはテキストが入りますここにはテキストが入りますここにはテキストが入ります。",
    date: '3 months ago'
  }]
};
exports.__BlogSinglePost = __BlogSinglePost;